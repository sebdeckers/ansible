# Ansible Playbooks for Commons Host

## Requirements

- [Ansible](https://www.ansible.com) to run the Playbooks. Only needed on the controller and for development, not the CDN servers.
- [VirtualBox](https://www.virtualbox.org) to run virtual machines. Only needed for development.
- [Vagrant](https://www.vagrantup.com) to manage virtual machines. Only needed for development.

## Installation

Use the following command to install Ansible Galaxy roles included in `requirements.yml`:

```
ansible-galaxy install -r requirements.yml
```

Note: On macOS additional dependencies are required to work with Vagrant.

```
brew install http://git.io/sshpass.rb
pip install passlib
```

## Usage

### Developing with Vagrant

Run `vagrant plugin install vagrant-triggers`

Run `vagrant up` the first time to create a Ubuntu 16.04 server. This automatically *provisions* using Ansible.

Run `vagrant provision` to execute the `vagrant.yaml` Ansible playbook which sets up the root user like the Odroid Ubuntu distribution defaults. This should not be needed since it only needs to be run once, which is done automatically by `vagrant up`.

Run `vagrant destroy` to delete the VM and start over.

Run Ansible playbooks against the Vagrant host in the inventory:

```
ansible-playbook -i hosts/pops.yaml -l vagrant pop.staging.yaml
```

### Initial Server Configuration

Sets up the authentication and other security policies.

```
ansible-playbook -i hosts/pops.yaml -l hostname bootstrap.yaml
```

Replace `hostname` with the name assigned to the server in the inventory.

Pass the `-k` option to interactively supply an override the SSH password for the `root` user. By default the password is `odroid`.

### Adding a User to Access the Vault

1. Add their GPG public key to `/keys/gpg` with the file name: first name + `.` + last name. E.g. `jane.doe`
1. Add their SSH public key to `/keys/ssh` with the file name: first name + `.` + last name. E.g. `jane.doe`
1. Edit the script `re-encrypt-vault` to add their email and and file name.
1. Ask an existing vault user to run `re-encrypt-vault`. Git commit and push the updated `vault-password.gpg` file.

### Monitoring Logs of the Systemd Edge Service

```
systemctl status edge.service
```

```
sudo journalctl -u edge -f -n 30
```

More tips:
https://www.digitalocean.com/community/tutorials/how-to-use-journalctl-to-view-and-manipulate-systemd-logs

### Deploying

The two playbooks for deployment are:

- `pop.staging.yaml`
- `pop.production.yaml`

To deploy to all pops:

```
ansible-playbook -i hosts/pops.yaml pop.staging.yaml -l odroid
```

Or, to deploy to a specific pop:

```
ansible-playbook -i hosts/pops.yaml pop.staging.yaml -l sg-sin-1a
```

Or, to deploy to pops in the same area:

```
ansible-playbook -i hosts/pops.yaml pop.staging.yaml -l 'sg-*'
```

## Colophon

Made with ❤️ by [Kenny Shen](https://machinesung.com) and [Sebastiaan Deckers](https://twitter.com/sebdeckers) in 🇸🇬 Singapore.
