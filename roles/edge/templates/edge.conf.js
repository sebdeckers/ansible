const {resolve} = require('path')

module.exports = {
  log: {
    level: 'trace'
  },
  // Directory containing static files for all sites
  store: {
    path: resolve(process.cwd(), process.env.COMMONSHOST_EDGE_STORE_PATH)
  },
  // Directory containing configuration options for all sites
  configurations: {
    path: resolve(process.cwd(), process.env.COMMONSHOST_EDGE_CONFIGURATIONS_PATH)
  },
  core: {
    api: process.env.COMMONSHOST_EDGE_CORE_API
  },
  pubnub: {
    channels: process.env.COMMONSHOST_EDGE_PUBNUB_CHANNELS.split(' '),
    uuid: process.env.COMMONSHOST_EDGE_PUBNUB_UUID,
    publishKey: process.env.COMMONSHOST_EDGE_PUBNUB_PUBLISH_KEY,
    subscribeKey: process.env.COMMONSHOST_EDGE_PUBNUB_SUBSCRIBE_KEY
  },
  s3: {
    s3cmd: process.env.COMMONSHOST_EDGE_S3_S3CMD,
    region: process.env.COMMONSHOST_EDGE_S3_REGION,
    endpoint: process.env.COMMONSHOST_EDGE_S3_ENDPOINT,
    bucket: process.env.COMMONSHOST_EDGE_S3_BUCKET,
    accessKeyId: process.env.COMMONSHOST_EDGE_S3_ACCESS_KEY_ID,
    secretAccessKey: process.env.COMMONSHOST_EDGE_S3_SECRET_ACCESS_KEY
  },
  auth0: {
    issuer: process.env.COMMONSHOST_EDGE_AUTH0_ISSUER,
    audience: process.env.COMMONSHOST_EDGE_AUTH0_AUDIENCE,
    clientId: process.env.COMMONSHOST_EDGE_AUTH0_CLIENT_ID,
    clientSecret: process.env.COMMONSHOST_EDGE_AUTH0_CLIENT_SECRET
  },
  // configuration object or filename: e.g. 'commonshost.conf.js'
  server: {
    log: {
      level: 'trace'
    },
    http: {
      from: parseInt(process.env.COMMONSHOST_EDGE_SERVER_HTTP_FROM),
      to: parseInt(process.env.COMMONSHOST_EDGE_SERVER_HTTP_TO)
    },
    https: {
      port: parseInt(process.env.COMMONSHOST_EDGE_SERVER_HTTPS_PORT),
      key: process.env.COMMONSHOST_EDGE_SERVER_HTTPS_KEY,
      cert: process.env.COMMONSHOST_EDGE_SERVER_HTTPS_CERT,
      ca: [process.env.COMMONSHOST_EDGE_SERVER_HTTPS_CA]
    },
    acme: {
      proxy: process.env.COMMONSHOST_EDGE_SERVER_ACME_PROXY,
      store: resolve(process.cwd(), process.env.COMMONSHOST_EDGE_SERVER_ACME_STORE),
      key: '$store/sites/$domain/crypto/key.pem',
      cert: '$store/sites/$domain/crypto/cert.pem'
    }
  }
}
